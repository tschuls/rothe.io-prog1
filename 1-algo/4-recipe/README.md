# 1.4 Rezept
---

Ein Rezept ist ein Algorithmus, welches den Prozess «Speise zubereiten» beschreibt. Kochrezepte sind in einer Fachsprache verfasst.

## Schritt

Die Einzelschritte eines Rezepts sind so vielfältig wie die Zubereitungstechniken beim Kochen, z.B.:

- «Eiweiss und Salz zu Schnee schlagen»
- «für 3 bis 5 Minuten backen»

## Wiederholung

Wiederholungen in Rezepten werden oft durch das Wort «**bis**» angedeutet wie in folgender Anweisung: «Weiterschlagen, bis die Masse glänzt»

Auch in der Anweisung «Sterne ausstechen» versteckt sich eine Wiederholung: Man soll solange einen Stern ausstechen, bis der ganze Teig aufgebraucht ist.

## Bedingte Ausführung

In Rezepten kommt eine bedingte Ausführung vor, wenn frei entschieden werden kann, ob und welche Zutaten verwendet werden sollen, z.B.:

- «500 g saisonales Gemüse»
- «würzen nach Belieben»

## Unterprogramm



## Parameter

Die gewünschte **Anzahl Portionen** ist der Parameter für jedes Rezept. Normalerweise werden die Mengen der Zutaten für vier Portionen angegeben. Wenn man mehr oder weniger Portionen zubereiten will, müssen die Mengen entsprechend umgerechnet werden.

## Beispiel[^1]

::: info Zimtsterne 

![Zimtsterne ©](./cinnamon-stars.jpg)

#### Zutaten

- 3 Eiweiss
- 1 Prise Salz
- 200 g Puderzucker
- 1 EL Zimt
- 0.5 EL Zitronensaft
- 350 g gemahlene Mandeln

#### Zubereitung

1. Eiweiss und Salz zu Schnee schlagen
2. Puderzucker dazu sieben und weiterschlagen, bis die Masse glänzt
3. Ein Drittel der Masse für die Glasur beiseite stellen
4. Zimt, Zitronensaft und Mandeln beifügen und mischen
5. Teig 1 cm dick auf Zucker auswallen
6. Sterne ausstechen
7. Glasieren
8. Auf mit Backpapier ausgelegtes Blech geben
9. Einige Stunden antrocknen lassen
10. In der Mitte des auf 250 ° C vorgeheizten Ofens für 3 bis 5 Minuten backen
:::


[^1]: Quelle: Tiptopf (1988), Staatlicher Lehrmittelverlag Bern
