# :extra: Datentypen
---

Wir wissen aus dem Kapitel «Codes und Daten», dass im Computer alle Daten in binärer Form dargestellt werden. Dies gilt auch für Variablen. Eine Variable entspricht einem bestimmten Bereich des Speichers. Dort ist eine Bitfolge abgelegt, z.B.

`01000001`

Die Bedeutung dieser Bitfolge ist nicht klar. Sie kann beispielsweise die Dezimalzahl 65 oder das ASCII-Zeichen «A» darstellen. Damit Python dies unterscheiden kann, wird mit jedem Wert zusätzlich sein **Typ** gespeichert.

## Typ ermitteln

~~~ python
type(x)
~~~
ermittelt den Typ des Werts von `x`. Einige Typen in Python sind:

| Typ        | Bedeutung                       |                Englisch |
|:---------- |:------------------------------- | -----------------------:|
| `int`      | ganze Zahl                      |               *integer* |
| `float`    | Fliesskommazahl                 | *floating point number* |
| `str`      | Text / Zeichenkette             |                *string* |
| `bool`     | Wahrheitswert                   |         *boolean value* |
| `function` | Typ eines Unterprogramms        |              *function* |
| `type`     | Typ eines Typs                  |                  *type* |

## Verhalten verschiedener Typen
