from turtle import *

def quadrat(seite):
    for i in range(4):
        forward(seite) 
        left(90)


pencolor("red")
quadrat(80)

left(180)
pencolor("green")
quadrat(50)