from turtle import *


def schneeflocke(länge, n):
    if n > 0:
        länge = länge / 3
        forward(2*länge)
        for i in range(6):
            schneeflocke(länge, n - 1)
            left(60)
        back(2*länge)
    else:
        forward(länge)
        back(länge)

schneeflocke(200, 2)
