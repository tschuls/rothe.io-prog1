# 2.8 Struktogramm
---

Struktogramme sind eine Möglichkeit, Programme und Algorithmen grafisch darzustellen. Diese Dartstellungsform wurde 1973 von Isaac Nassi und Ben Shneiderman vorgeschlagen und heisst deshalb auch Nassi-Shneiderman-Diagramm.

## Sequenz

In einem Struktogramm wird jeder Befehl in einen rechteckigen Kasten geschrieben. Befehle müssen nicht in einer Programmiersprache geschrieben werden.

::: columns 2
``` python
forward(141)
left(135)
forward(100)
left(90)
forward(100)
```

***
![Struktogramm Sequenz](./nsd-sequence.svg)
:::


## Wiederholung

Eine Wiederholung wird folgendermassen dargestellt:

::: columns 2
``` python
for i in range(4):
    forward(100)
    left(90)
```

***
![Struktogramm Wiederholung](./nsd-loop.svg)
:::

## Unterprogramme

::: columns 2
``` python
def quadrat():
    for i in range(4):
        forward(100)
        left(90)

pencolor("red")
quadrat()
left(180)
pencolor("blue")
quadrat()
```

***
![Struktogramm Unterprogramm](./nsd-subroutine.svg)
:::
