from turtle import *

shape("turtle")

# grosses Quadrat
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)

# Übergang
left(90)
forward(50)
left(45)

# kleines Quadrat
forward(71)
left(90)
forward(71)
left(90)
forward(71)
left(90)
forward(71)
