# 2.9 Ereignisse
---

## Automatische Bewegung

Für die folgende Anwendung benötigen wir eine Turtle, die sich automatisch vorwärts bewegt. Dazu setzen wir eine neue Art von Wiederholung ein. Mit einer `while`-Schleife können Befehle unendlich oft wiederholt werden.

Im folgenden Beispiel bewegt sich die Turtle endlos vorwärts:

``` python ./ereignis1.py
```

## Ereignisse

Ereignisse unterbrechen den normalen Programmablauf. Typisch reagiert ein Programm auf Benutzereingaben, z.B. Mausklicks oder eine Tastatureingabe. Wir können so z.B. eine Turtle erstellen, welche sich mit den Tasten der Tastatur steuern lässt.

Ereignisse muss man aber registrieren: Die Turtle muss wissen, welches Unterprogramm aufgerufen werden soll, wenn beispielsweise die Taste [A] gedrückt wird. Dies geschieht mit dem `onkey`-Befehl.

Im folgenden Beispiel wird das Unterprogramm `links` aufgerufen, wenn die Taste [Left] gedrückt wird. Mit dem Befehl `listen()` wird der Turtle mitgeteilt, dass sie auf Tastaturanschläge achten soll.

``` python ./ereignis2.py
```

Um auf eine «normale» Taste zu reagieren, wird das entsprechende Zeichen in Anführungszeichen geschrieben, beispielsweise `"a"` für die Taste [A].

Für Spezialtasten können folgende Namen verwendet werden:

| Wert        | Taste   | Beschreibung          |
|:----------- |:------- |:--------------------- |
| `"Left"`    | [Left]  | Links-Taste           |
| `"Right"`   | [Right] | Rechts-Taste          |
| `"Up"`      | [Up]    | Oben-Taste            |
| `"Down"`    | [Down]  | Unten-Taste           |
| `" "`       | [Space] | Leertaste             |
| `"Shift_L"` | [Shift] | linke Hochstelltaste  |
| `"Shift_R"` | [Shift] | rechte Hochstelltaste |


::: exercise Aufgabe 1 – Steuerung

Erweitere das obenstehende Beispiel so, dass man die Turtle mit der Taste [Right] nach rechts drehen kann.

***

``` python solutions/exercise1.py
```

:::


::: exercise Aufgabe 2 – Zeichnungsprogramm
Erweitere deine Lösung der Aufgabe 1 zu einem Zeichnungsprogramm.

Folgendes wäre denkbar:
- die Turtle ist steuerbar mit der Tastatur (ev. ohne Geschwindigkeit und `while`-Schleife, sondern mit Cursortaste nach oben für vorwärts)
- einige Tasten setzen verschiedene Farben (`pencolor`, `fillcolor`)
- eine Taste macht ein `begin_fill`, eine andere ein `end_fill`
- mit den Zahlen-Tasten lässt sich die Stiftdicke einstellen, z.B. `turtle.pensize(5)`
- Spezialtasten zeichnen ganze Kreise oder in Unterprogrammen gespeicherte Figuren, z.B. ein Stern
- `penup`, `pendown`
- ...
:::
