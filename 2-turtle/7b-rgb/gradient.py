from turtle import *

speed("fastest")
hideturtle()

def rechteck():
    begin_fill()
    forward(20)
    left(90)
    forward(100)
    left(90)
    forward(20)
    left(90)
    forward(100)
    left(90)
    end_fill()

back(300)
for i in range(30):
    farbe = i / 30, 1, 0
    color(farbe)
    rechteck()
    forward(20)
