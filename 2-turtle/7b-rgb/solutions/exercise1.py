from turtle import *

speed("fastest")
hideturtle()

def rechteck():
    begin_fill()
    forward(20)
    left(90)
    forward(100)
    left(90)
    forward(20)
    left(90)
    forward(100)
    left(90)
    end_fill()

back(300)
for i in range(30):
    farbe = 1, 0, i / 30
    color(farbe)
    rechteck()
    forward(20)
