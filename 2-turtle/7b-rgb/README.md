# :extra: RGB-Farben
---

Wir wissen, das im Computer Farben im RGB-Farbmodell dargestellt werden. Dabei wird für die Grundfarben Rot, Grün und Blau jeweils eine Helligkeit festgelegt.

Die Turtle akzeptiert ebenfalls RGB-Werte als Farbe. Der RGB-Wert kann z.B. mit dem folgenden Tool bestimmt werden:

<VueColourPicker percent output="tuple"/>

Das Farbtupel kann nun in den Turtle-Befehlen `pencolor` oder `fillcolor` verwendet werden:

``` python
fillcolor(1.0, 0.8, 0.5)
```

## Farbverlauf

In einer `for`-Schleife zählt die Variable `i` die Anzahl Durchläufe der Schleife. Wir können diese Variable `i` verwenden, um beispielsweise den Rotanteil der Zeichenfarbe von 0 bis 1 zu ändern.

Das folgende Programm zeichnet 30 Rechtecke. Bei jedem Durchgang wird der Rotanteil der Zeichenfarbe abhängig von `i` berechnet. So ergibt sich ein Farbverlauf von der Farbe Grün (0, 1, 0) zu der Farbe Gelb (1, 1, 0).

::: columns 2
``` python gradient.py
```
***
![](./gradient.png)
:::

:::  exercise Aufgabe 1 – Farbverlauf
Passe das obenstehende Turtle-Programm so an, dass ein anderer Farbverlauf gezeichnet wird, beispielsweise ein Verlauf von Rot (1, 0, 0) nach Violett (1, 1, 0).

![](./exercise1.png)
***
``` python solutions/exercise1.py
```
:::

## Farbkreis

Im Farbkreis werden die gesättigten Farben in einem Kreis angeordnet, wobei ähnliche Farben nebeneinander liegen. Komplementärfarben liegen sich oft gegenüber.

::: columns 2
![HSV-Farbkreis](./hsv-circle.svg)
***
![Ittens Farbkreis ©](./colour-wheel-itten.svg)
:::

## HSV-Farbmodell

Hier verwenden wir den Farbkreis, welcher auf dem HSV-Farbmodell beruht. Dabei wird die Farbe durch den **Farbwinkel** (engl. *hue*) angegeben. Ausserdem werden **Sättigung** (engl. *saturation*) und **Helligkeit** (engl. *value*) angegeben. Hier betrachten wir aber nur den Farbwinkel.

Die Umrechnung eines Winkels $h$ des Farbkreises in einen RGB-Farbwert geschieht gemäss nachfolgender Tabelle. Dabei ist $v = \frac{h}{60}$.

| Winkel            |   Rot   |  Grün   |  Blau   |
| ----------------- |:-------:|:-------:|:-------:|
| $0° ≤ h < 60°$    |   $1$   |   $v$   |   $0$   |
| $60° ≤ h < 120°$  | $2 - v$ |   $1$   |   $0$   |
| $120° ≤ h < 180°$ |   $0$   |   $1$   | $v - 2$ |
| $180° ≤ h < 240°$ |   $0$   | $4 - v$ |   $1$   |
| $240° ≤ h < 300°$ | $v - 4$ |   $0$   |   $1$   |
| $300° ≤ h < 360°$ |   $1$   |   $0$   | $6 - v$ |


## Python

Das folgende Python-Unterprogramm berechnet eine RGB-Farbe aus einem Winkel.

``` python
def winkelfarbe(h):
    v = h / 60
    if h < 60:
        return 1, v, 0
    elif h < 120:
        return 2 - v, 1, 0
    elif h < 180:
        return 0, 1, v - 2
    elif h < 240:
        return 0, 4 - v, 1
    elif h < 300:
        return v - 4, 0, 1
    else:
        return 1, 0, 6 - v
```

::: exercise Aufgabe 2 – Farbkreis
Erstelle ein Python-Programm, welches den HSV-Farbkreis zeichnet. Verwende dazu das obenstehende Unterprogramm `winkelfarbe`.

![](./exercise2.png)
***
Verwende das folgende Unterprogramm, um einen Kreissektor zu zeichnen:
``` python
def sektor(radius, winkel):
    begin_fill()
    forward(radius)
    left(90)
    circle(radius, winkel)
    left(90)
    forward(radius)
    left(180)
    end_fill()
```
***
``` python solutions/exercise2.py
```
:::

::: exercise Aufgabe 3 – Regenbogen
Erstelle ein Python-Programm, welches einen Regenbogen zeichnet. Verwende dazu das obenstehende Unterprogramm `winkelfarbe`.

![](./exercise3.png)
***
``` python solutions/exercise3.py
```
:::
