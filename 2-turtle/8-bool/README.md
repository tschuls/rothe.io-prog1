# 2.8 Wahrheitswerte
---

In Programmen möchten wir entscheiden können, ob Befehle ausgeführt werden sollen oder nicht. Auch möchten wir entscheiden können, ob eine Wiederholung abgebrochen werden soll. Wir möchten folgendes in Python ausdrücken können:

> «Gehe mit der Turtle vorwärts, bis sie 100 Pixel von der Fenstermitte entfernt ist.»
>
> «Zeichne acht Quadrate und fülle jedes zweite in Schwarz.»

## Wahr und Falsch

Um zu entscheiden, ob Befehle ausgeführt werden soll, wird immer eine Bedingung überprüft. In Programmiersprachen ergibt eine Bedingung immer einen sogenannten **Wahrheitswert**. Es gibt in Python die folgenden zwei Wahrheitswerte:

| Wert    | Bedeutung |
|:------- |:--------- |
| `True`  | wahr      |
| `False` | falsch    |

::: warning Achtung
`True` und `False` werden mit einem grossen Anfangsbuchstaben geschrieben.
:::

## Vergleiche

Ein Wahrheitswert entsteht auch durch einen Vergleich. Beispielsweise hat `4 < 2` den Wahrheitswert `False`. Der Wahrheitswert von `x < 2` hängt davon ab, welcher Wert zur Zeit in der Variable `x` gespeichert ist.

In Python können alle üblichen Vergleiche geschrieben werden.

| Name             | Mathematisch | Python |
| ---------------- |:------------:|:------:|
| *grösser*        |    $\lt$     |  `<`   |
| *kleiner*        |    $\gt$     |  `>`   |
| *grösser gleich* |    $\leq$    |  `<=`  |
| *kleiner gleich* |    $\geq$    |  `>=`  |
| *gleich*         |     $=$      |  `==`  |
| *ungleich*       |    $\neq$    |  `!=`  |

::: warning Achtung
Bei der Überprüfung auf Gleichheit werden zwei Gleichheitszeichen `==` geschrieben.
:::

## Wiederholung mit Bedingung

Eine Wiederholung mit Bedingung führt Anweisungen so lange aus, bis die Bedingung den Wahrheitswert `True` ergibt.

~~~ python
while Bedingung:
    Anweisung
~~~
Das bedeutet: «Führe die Anweisung so lange aus, bis die Bedingung den Wahrheitswert `True` hat.»

Im folgenden Beispiel wird in einer Schleife die Variable `w` hochgezählt, bis sie nicht mehr kleiner als 60 ist:

::: columns 2
``` python kreis.py
```

***
![](./kreis.png)
:::

Natürlich könnte man die auch mit einer `for`-Schleife erreichen. Oft weiss man aber nicht genau, wann ein bestimmter Wert erreicht ist.

Im folgenden Beispiel wird die Variable `w` mit 0.99 multipliziert, so lange der Wert von `w` grösser als 2 ist. `w` wird als Winkel verwendet, um welchen die Turtle dreht. Somit ergibt sich eine Spirale:

::: columns 2
``` python spirale.py
```

***
![](./spirale.png)
:::


## Verzweigung

Mit einer **Verzweigung** können Anweisungen nur unter bestimmten Bedingungen ausgeführt werden.

~~~ python
if Bedingung:
    Anweisung
~~~

Das bedeutet: «Führe die Anweisung nur aus, wenn die Bedingung den Wahrheitswert `True` hat.»

Verzweigungen machen vor allem Sinn, wenn Sie in einer Schleife oder einem Unterprogramm verwendet werden, also in einem Abschnitt des Programms, der mehrmals ausgeführt werden kann.

Im folgenden Beispiel wird die letzte Seite eines Quadrats rot gezeichnet.

::: columns 2
``` python quadrat.py
```

***
![](./quadrat.png)
:::

::: exercise Aufgabe 1

Erstelle ein Programm, welches ein regelmässiges Sechseck zeichnet. Die ersten zwei Seiten sollen grün, die zweiten zwei rot und die letzten zwei blau gezeichnet werden. Verwende dazu die Verzweigung.

![](./exercise1.png)
:::

## Verzweigung mit Alternative

::: columns 2
``` python quadrat2.py
```
***
![](./quadrat2.png)
:::

Nun lernen wir einen neuen Turtle-Befehl kennen. Im Gegensatz zu den bisherigen Turtle-Befehlen beeinflusst er die Turtle nicht. Er ergibt aber einen Wahrheitswert, den man als verwenden kann:

~~~ python
isdown()
~~~
überprüft, ob der Stift gesenkt ist. Damit kann man schreiben:

``` python
if isdown():
    # wenn der Stift gesenkt ist...
else:
    # wenn der Stift oben ist...
```

::: exercise Aufgabe 2 - Gestrichelte Linie
Erweitern Sie das folgende Programm so, dass eine gestrichelte Linie gezeichnet
wird:

``` python
from turtle import *

for i in range(10):
    forward(10)
```
:::
