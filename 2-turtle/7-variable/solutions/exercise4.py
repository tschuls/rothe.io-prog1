from turtle import *
from tkinter.simpledialog import *

def quadrat(seite):
    for i in range(4):
        forward(seite)
        left(90)

speed("fastest")
seite = askinteger("Eingabe", "Seitenlänge:")
for i in range(20):
    quadrat(seite)
    seite = seite * 0.9
    left(10)
