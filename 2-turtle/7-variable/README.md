# 2.7 Variablen
---

## Einführung

Im vorhergehenden Kapitel hast du Quadrate gezeichnet, deren Seitenlänge im Programm fest eingebaut waren. Manchmal möchtest du aber die Seitenlänge mit einem Eingabedialog einlesen. Dazu muss das Programm die eingegebene Zahl als Variable speichern. Du kannst eine **Variable** als einen Behälter auffassen, auf dessen Inhalt du mit einem Namen zugreifst. Kurz gesagt hat eine Variable einen **Namen und einen Wert**. Den Namen der Variablen darfst du frei wählen. Nicht erlaubt sind Schüsselwörter und Namen mit Sonderzeichen. Zudem darf der Name nicht mit einer Zahl beginnen.

Mit der Schreibweise `a = 42` erstellst du den Behälter, auf den  du mit dem Namen `a` zugreifst und legst die Zahl 42 hinein. In Zukunft sagen wir, dass du damit eine Variable a **definierst** und ihr einen Wert **zuweist**.

![a = 42](./variable42.png)

Du kannst in den Behälter nur ein einziges Objekt legen. Wenn du später unter dem Namen a die Zahl 8 speichern willst, so schreibst du `a = 8`.

![a = 8](./variable8.png)

Was geschieht, wenn du nun `a = a + 4` schreibst? Du  nimmst die Zahl, die sich gegenwärtig im Behälter befindet, auf den du mit a zugreifst, also die Zahl 8 und addierst dazu 4. Das Resultat 12 speicherst du wieder unter dem Namen a.

![a = a + 4](./variable-add.png)

Das Gleichheitszeichen hat also in Computerprogrammen nicht dieselbe Bedeutung wie in der Mathematik. Es ist keine Gleichung, sondern eine Variablendefinition oder eine Zuweisung.

## Variablenwert einlesen und verändern

Im Programm kannst du mit Hilfe einer Eingabebox der Variablen x einen Wert zuweisen. Diesen Wert veränderst du nachfolgend in der Wiederholstruktur und zeichnest dadurch eine Spirale.

::: columns 2
``` python spiral.py
```
***
![](./askinteger.png)
:::


::: info

Mit dem Modul `tkinter.simpledialog` können Eingaben vom Benutzer eingelesen werden.
Im Python-Programm müssen zunächst die Befehle importiert werden:

``` python
from tkinter.simpledialog import *
```

Die Eingabeboxen funktionieren nur, wenn ein Turtle-Fenster sichtbar ist. Bevor eine Eingabebox verwendet wird, muss also zuerst das Turtle-Fenster angezeigt werden. Das geschieht, indem ein beliebiger Turtle-Befehl aufgerufen wird, z.B.

``` python
speed("fast")
```

Danach können Eingabeboxen verwendet werden. Als Parameter wird der Titel der Eingabebox und der Text angegeben, welcher dem Benutzer angezeigt wird:

``` python
x = askinteger("Eingabe", "Gib eine ganze Zahl ein:")
```

Mit **Variablen** kannst du Werte speichern, die du im Laufe des Programms lesen und verändern kannst. Jede Variable hat einen Namen und einen Wert. Mit dem Gleichheitszeichen definierst du eine Variable und weist ihr einen Wert zu.

Die Grundrechenoperationen werden in Python folgendermassen geschrieben:

| Operation      | mathematisch | Python   |
|:-------------- |:------------ |:-------- |
| Addition       | $a + b$      | `a + b`  |
| Subtraktion    | $a - b$      | `a - b`  |
| Multiplikation | $a \cdot b$  | `a * b`  |
| Division       | $a ÷ b$      | `a / b`  |
| Potenz         | $a^b$        | `a ** b` |

:::

## Unterschiede zwischen Variablen und Parameter

Du solltest zwischen einer Variablen und einem Parameter unterscheiden. Parameter sind nur innerhalb eines Unterprogramms gültig und transportieren Daten in ein Unterprogramm, während Variablen überall möglich sind. Beim Aufruf erhält der Parameter einen Wert und kann im Innern des Unterprogramms wie eine Variable verwendet werden.

Um den Unterschied klar zu machen, verwendest du in deinem Programm im Unterprogramm `quadrat` den Parameter `seite`. Mit einem Eingabedialog liest du eine Zahl ein und speichert sie in der Variablen `s`. Beim Aufruf von `quadrat` übergibst du dem Parameter `seite` den Variablenwert von `s`.

``` python square1.py
```

::: info
Du musst zwischen der Variable `s` und dem Parameter `seite` unterscheiden. Parameter sind in der Funktionsdefinition Platzhalter und können beim Aufruf wie Variablen aufgefasst werden, die nur im Inneren der Funktion bekannt sind.

Ruft man die Funktion mit einer Variablen auf, so wird der Variablenwert in der Funktion benutzt. `quadrat(laenge)` zeichnet somit ein Quadrat mit der Seitenlänge `laenge`.
:::

## Gleiche Namen für verschiedene Dinge

Wie du weisst, sollen Parameter- und Variablennamen ausdrücken, auf was sie sich beziehen. Sie sind aber eigentlich frei wählbar. Deshalb ist es üblich, bei gleichen Bezügen den gleichen Namen für **Parameter** und **Variablen** zu wählen. Es ergeben sich dadurch im Programm keine Namenskonflikte, du musst aber zum Verständnis des Programms den Unterschied im Auge behalten.

``` python square2.py
```

::: info
Auch wenn du für gewisse Parameter und Variablen den gleichen Namen verwendest, solltest du  **Parameter** und **Variablen** begrifflich auseinander halten.
:::

::: exercise Aufgabe 1 – n-Eck
Nach Eingabe der Anzahl Ecken in einer Eingabebox soll die Turtle ein regelmässiges n-Eck zeichnen. Beispielsweise wird nach der Eingabe 8 ein 8-Eck gezeichnet. Den passenden Drehwinkel soll das Programm berechnen. Spiele dazu Turtle und überlege dir, wie weit du dich drehen musst, um die nächste Seite zu zeichnen. Erinnere dich an das Zeichnen eines gleichseitigen Dreiecks.

![](./exercise1-input.png) ![](./exercise1.png)
***
``` python solutions/exercise1.py
```
:::

::: exercise Aufgabe 2
Nach der Eingabe eines Winkels in einer Eingabebox zeichnet die Turtle 30 Strecken der Länge 100, wobei sie nach jeder Strecke um den gegebenen Winkel nach links dreht. Experimentiere mit verschiedenen Winkeln und zeichne schöne Figuren. Mit `speed("fastest")` kannst du das Zeichnen beschleunigen.

![](./exercise2.png)
***
``` python solutions/exercise2.py
```
:::

::: exercise Aufgabe 3
Die Turtle soll 10 Quadrate zeichnen. Definiere zuerst einen Befehl `quadrat` mit dem Parameter `seite`. Die Seitenlänge des ersten Quadrats ist 8. Bei jedem weiteren Quadrat ist die Seitenlänge um 10 grösser.

![](./exercise3.png)
***
``` python solutions/exercise3.py
```
:::

::: exercise Aufgabe 4
Du kannst in einer Eingabebox die Seitenlänge des grössten Quadrats eingeben. Die Turtle zeichnet dann 20 Quadrate. Nach jedem Quadrat wird die Seitenlänge um den Faktor 0.9 kleiner und die Turtle dreht um den Winkel 10° nach links.

![](./exercise4.png)
***
``` python solutions/exercise4.py
```
:::
