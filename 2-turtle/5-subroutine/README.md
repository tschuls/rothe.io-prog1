# 2.5 Unterprogramme
---

## Einführung

In einem grösseren Bild kommen Figuren wie Dreiecke und Quadrate mehrmals vor. Die Turtle weiss aber nicht, was ein Dreieck oder ein Quadrat ist. Du musst also jedes Mal der Turtle mit einem vollständigen Programmcode erklären, wie sie die Figuren zeichnet. Geht das nicht auch einfacher?

Es geht einfacher! Du kannst der Turtle nämlich neue Befehle beibringen, z.B.ein Quadrat oder Dreieck zu zeichnen, und musst ihr dann nur sagen, sie soll diesen Befehl ausführen, also ein Quadrat oder ein Dreieck zeichnen. Um einen neuen Befehl zu definieren, wählst du einen beliebigen Namen, beispielsweide `quadrat`, und schreibst `def quadrat()`: Danach notierst du alle Anweisungen, die zum neuen Befehl gehören. Damit der Computer weiss, was zum neuen Befehl gehört, müssen diese Anweisungen eingerückt sein.

Programmierkonzepte: Modulares Programmieren, Definition und Aufruf von Unterprogrammen

## Eigene Befehle definieren

In diesem Programm definierst du mit def den neuen Befehl `quadrat()`. Die Turtle weiss danach, wie sie ein Quadrat zeichnen kann, sie hat aber noch keines gezeichnet.

Mit dem Befehl `quadrat()` zeichnet die Turtle an jeder aktuellen Position ein Quadrat mit der Seitenlänge 100. In unserem Beispiel ist es ein rotes, ein blaues und ein grünes Quadrat.

::: columns 2
``` python squares.py
```
***
![](./squares.png)
:::

::: info
Mit `def name():` definierst du einen neuen Befehl. Wähle einen Namen, der die Tätigkeit wiederspiegelt. Alle Anweisungen, die zum neuen Befehl gehören, müssen eingerückt sein.

~~~ python
def name():
    Anweisungen
~~~

Vergiss die Klammern und den Doppelpunkt nach dem Namen nicht! In Python nennt man neue Befehle auch **Unterprogramme** oder **Funktionen**. Wenn du das Unterprogramm `quadrat()` verwendest, sagt man auch, das Unterprogramm werde «aufgerufen».
:::


::: exercise Aufgabe 1 – Sechsecke
Definiere einen Befehl `sechseck()`, mit dem du ein Sechseck zeichnen kannst. Verwende diesen Befehl, um die folgende Figur zu zeichnen, welche aus 10 Sechsecken besteht:

![](./exercise1.png)
***
``` python solutions/exercise1.py
```
:::

::: exercise Aufgabe 2 – Quadrate
1. Definiere einen Befehl für ein Quadrat, das auf der Spitze steht und zeichne damit die folgende Figur:

    ![](./exercise2a.png)

2. Du kannst gefüllte Quadrate zeichnen, indem du die Befehle `begin_fill()` und `end_fill()` verwendest.

    ![](./exercise2b.png)
:::

::: exercise Aufgabe 3 – Blume

Du erlebst in dieser Aufgabe, wie du unter Verwendung von Unterprogrammen eine komplexe Aufgabe schrittweise lösen kannst.

1. Definiere ein Unterprogramm `bogen(),` mit dem die Turtle einen Bogen zeichnet und sich dabei insgesamt um 90 Grad nach rechts dreht. Mit `speed("fastest")` kannst du die Turtlegeschwindigkeit auf den maximalen Wert setzen.

    ![](./exercise3a.png)

2. Ergänze das Programm mit dem Unterprogramm `blumenblatt()`, welches zwei Bogen zeichnet. Die Turtle sollte am Ende aber wieder in Startrichtung stehen.

    ![](./exercise3b.png)

3. Ergänze das Programm so, dass `blumenblatt()` ein rot gefülltes Blatt zeichnet. Setze mit `pencolor("red")` die Stiftfarbe auf rot, damit die Umrandungslinie die gleiche Farbe hat.

    ![](./exercise3c.png)

4. Erweitere das Programm mit dem Unterprogramm `blume()` so, dass eine fünfblättrige Blume gezeichnet wird.

    ![](./exercise3d.png)
***
``` python solutions/exercise3d.py
```
:::

::: extra Zusatzaufgabe 4 – Blume mit Stiel
Ergänze die Blume mit einem Stiel.

![](./exercise4.png)
***
``` python solutions/exercise4.py
```
:::
