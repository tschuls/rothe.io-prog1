# Lernziele
---

::: goal Lernziele Algorithmen

- Du kannst erklären, was ein Algorithmus ist.
- Du erkennst und beschreibst Strukturen eines Algorithmus (Einzelschritte, Wiederholungen, bedingte Ausführung, Parameter)
- Du übersetzt einen Algorithmus zwischen zwei Darstellungsformen (Alltagssprache, Diagramm, Python-Programm)
:::

::: goal Lernziele Programme
- Du kannst ein einfaches Python-Programm lesen und verstehen.
- Du kannst ein einfaches Python-Programm ändern oder erweitern.
- Du kannst typische Fehler in Python-Programmen erkennen und korrigieren.
:::

::: goal Lernziele Programmstuktur
- Du weisst, dass ein Python-Programm grundsätzlich sequentiell (d.h. zeilenweise) ausgeführt wird.
- Du kannst Befehle aus anderen Modulen **importieren** (`import`).
- Du verwendest `for`-**Schleifen**, um Befehle zu wiederholen.
- Du kannst eigene **Unterprogramme** / Befehle  definieren (`def`).
- Du kannst Unterprogramme mit einem oder mehreren **Parametern** definieren und aufrufen.
- Du verwendest **Verzweigungen** (`if`), um Befehle nur unter bestimmten Bedingungen auszuführen.
- Du weisst, was **Kommentare** (`#`) sind.
- <del>Du kannst **Variablen** verwenden.</del>
:::

::: goal Lernziele Turtlegrafik
- Du kannst die Turtle mit `forward`, `back`, `left` und `right` bewegen.
- Du kannst mit `pencolor` die Farbe ändern.
- Du kannst mit `fillcolor`, `begin_fill` und `end_fill` ausgefüllte Figuren zeichnen.
- Du kannst mit `shape` und `speed` das Aussehen und die Geschwindigkeit der Turtle ändern.
- Du kannst mit `pensize`, `penup` und `pendown` den Zeichenstift kontrollieren.
:::

## Grundlagen

Diese Unterrichtseinheit deckt die folgenden Grobziele und Inhalte aus dem kantonalen Lehrplan[^1] ab:

> #### Grobziele[^1]
>
> Die Schülerinnen und Schüler
> - sind vertraut mit Grundelementen zur Beschreibung und Visualisierung von Algorithmen und setzen diese ein
> - können einfache Algorithmen und Programme nachvollziehen und auf Fehler untersuchen
> - finden für einfache Aufgabenstellungen algorithmische Lösungen und können diese in einer Programmiersprache implementieren
> - können die praktische Umsetzbarkeit von Algorithmen einschätzen
>
> #### Inhalte
>
> - Darstellungsformen von Programmabläufen (z.B. Flussdiagramme, Struktogramme)
> - Algorithmus: Konzept, Definition, Entwurf
> - Programmentwicklung und Umgang mit Fehlern
> - Grundkenntnisse in einer Programmiersprache (Variable, Verzweigung, Schleife, Prozedur/Funktion)
> - Einfache Simulationen (z.B. Spiele, Automaten, Populationen, Zufallsexperimente)
> - Komplexität

[^1]: Quelle: *[Lehrplan 17 für den gymnasialen Bildungsgang - Informatik][1], Erziehungsdirektion des Kantons Bern, S. 145 - 146*

[1]: https://www.erz.be.ch/erz/de/index/mittelschule/mittelschule/gymnasium/lehrplan_maturitaetsausbildung.html
